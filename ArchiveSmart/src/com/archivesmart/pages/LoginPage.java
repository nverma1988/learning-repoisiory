package com.archivesmart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;

public class LoginPage {
	
	WebDriver driver;
	
	public static By email=By.name("email");
	static By next=By.xpath("/html/body/div/div/div[2]/form/div[2]/div/div/div[2]/div/button");
	static By password=By.name("password");
	static By loginbutton=By.cssSelector(".button-link");
	

	public LoginPage(WebDriver driver) {
		
		
		this.driver = driver;
		
	}



	 public void validLogin(String username,String pwd) throws InterruptedException
	
	{
		driver.findElement(email).sendKeys(username);
		driver.findElement(next).click();
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(email).sendKeys(Keys.RETURN);
		Thread.sleep(5000);
		WebElement element = driver.findElement(By.cssSelector("div.header-user-name"));
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		Thread.sleep(1000);
		driver.findElement(By.linkText("Exit")).click();
		
	}
		
		
		
		// driver.findElement(By.xpath("//*[class='icon-more-active']")).click();
		/*Actions make  = new Actions(driver);
		 Action kbEvents = make.keyDown(Keys.RETURN).build();
		 kbEvents.perform();*/

}	
		
		
		
	


	