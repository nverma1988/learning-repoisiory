package com.archivesmart.Testcase;

import org.testng.annotations.Test;

import com.archivesmart.pages.LoginPage;

import Helper.BrowserFactory;
import Helper.BrowserFactory2withInhertance;

public class VerifyLoginwithInheritance extends BrowserFactory2withInhertance {
		
	@Test
	public void checkValidUser() throws InterruptedException

	{
		BrowserFactory.start_Browser("FF", "http://ec2-13-56-11-114.us-west-1.compute.amazonaws.com:3028/login");
		Thread.sleep(3000);

		LoginPage login = new LoginPage(driver);

		login.validLogin("archivesmart.debut@gmail.com", "noahdebut@123");

	}

}
